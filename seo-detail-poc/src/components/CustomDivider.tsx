import React from 'react';
import { Divider } from '@care/react-component-lib';
import { Container } from '@material-ui/core';

const CustomDivider = () => {
  return (
    <>
      <Container maxWidth="md">
        <Divider />
      </Container>
    </>
  );
};

export default CustomDivider;
