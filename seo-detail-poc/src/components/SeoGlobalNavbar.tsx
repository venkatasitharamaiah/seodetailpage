import React from 'react';
import { Grid, Hidden, List, ListItem } from '@material-ui/core';
import IconCareLogo from '@care/react-icons/IconCareLogo';
import { makeStyles } from '@material-ui/core/styles';
import { theme } from '@care/material-ui-theme';
import Link from 'next/link';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const styles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(1, 5),
    backgroundColor: theme.palette.care.white,
    boxShadow: `0 2px 0 -1px ${theme.palette.care.grey[300]}`,
    position: 'sticky',
    top: 0,
    minHeight: '51px',
    height: 'auto',
    zIndex: 1050,
  },
  contentWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  seoLinks: {
    display: 'flex',
  },
}));

const SeoGlobalNavbar = () => {
  const headerClasses = styles();
  const isSmallerThanSM = useMediaQuery(theme.breakpoints.down('sm'));

  const careLogoSizes = () => {
    const sizeProps: { height: string; width: string } = {
      height: '',
      width: '',
    };

    if (isSmallerThanSM) {
      sizeProps.height = '32px';
      sizeProps.width = '116px';
    } else {
      sizeProps.height = '40px';
      sizeProps.width = '144px';
    }

    return sizeProps;
  };
  return (
    <>
      <header className={headerClasses.root}>
        <div className={headerClasses.contentWrapper}>
          <Grid container>
            <Grid item xs={3}>
              <Link href="/">
                <IconCareLogo {...careLogoSizes()} />
              </Link>
            </Grid>
            <Hidden xsDown>
              <Grid item xs={9}>
                <List className={headerClasses.seoLinks}>
                  <ListItem>CHILD CARE</ListItem>
                  <ListItem>TUTORING</ListItem>
                  <ListItem>SENIOR CARE</ListItem>
                  <ListItem>PET CARE</ListItem>
                  <ListItem>HOUSEKEEPING</ListItem>
                  <ListItem>HOW IT WORKS</ListItem>
                  <ListItem>GUIDES</ListItem>
                </List>
              </Grid>
            </Hidden>
          </Grid>
        </div>
      </header>
    </>
  );
};

export default SeoGlobalNavbar;
