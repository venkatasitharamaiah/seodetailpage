import React from 'react';
import {
  Icon24ShadedInfoDateNight,
  Icon24ShadedInfoTransportation,
  Icon24ShadedInfoHousekeeping,
  Icon24ShadedInfoErrands,
  Icon24ShadedInfoCompanion,
  Icon24ShadedInfoLaundry,
  Icon24ShadedInfoPackingMoving,
  Icon24ShadedInfoMealPrep,
} from '@care/react-icons';
import { Container, Grid } from '@material-ui/core';
import sideJobsStyles from '../styles/sideJobs.style';

const sideJobsList = [
  { name: 'Date Night', icon: <Icon24ShadedInfoDateNight /> },
  { name: 'Transportation', icon: <Icon24ShadedInfoTransportation /> },
  { name: 'Light Cleaning', icon: <Icon24ShadedInfoHousekeeping /> },
  { name: 'Groceries / Errands', icon: <Icon24ShadedInfoErrands /> },
  { name: 'Companion', icon: <Icon24ShadedInfoCompanion /> },
  { name: 'Organizing / Laundry', icon: <Icon24ShadedInfoLaundry /> },
  { name: 'Packing / Moving', icon: <Icon24ShadedInfoPackingMoving /> },
  { name: 'Meal Prep', icon: <Icon24ShadedInfoMealPrep /> },
];

const SideJobs = () => {
  const sideJobClasses = sideJobsStyles();

  return (
    <>
      <Container maxWidth="md">
        <h4 className={sideJobClasses.sectionTitle}>Abigail Can Also Help With:</h4>
        <div className={sideJobClasses.sideJobsSection}>
          <Grid container>
            {sideJobsList.map((sideJob) => {
              return (
                <Grid item xs={6} sm={3}>
                  <div className={sideJobClasses.sideJobs}>
                    <div className={sideJobClasses.sideJob}>
                      <div className={sideJobClasses.sideJobIcon}>{sideJob.icon}</div>
                    </div>
                    <div className={sideJobClasses.sideJobName}>{sideJob.name}</div>
                  </div>
                </Grid>
              );
            })}
          </Grid>
        </div>
      </Container>
    </>
  );
};

export default SideJobs;
