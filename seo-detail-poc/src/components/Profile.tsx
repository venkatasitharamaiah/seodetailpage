import React from 'react';
import { Button, Container, Grid } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import { Divider, Rating } from '@care/react-component-lib';
import profileStyles from '../styles/profile.style';

const Profile = () => {
  const profileClasses = profileStyles();
  return (
    <Container maxWidth="md">
      <Grid container>
        <Grid item xs={12} sm={6} className={profileClasses.profileSection}>
          <Avatar
            variant="square"
            src="https://d1z3744m6z2h75.cloudfront.net/aws/photo/450X450/52/23483452_0O5Ao9MirXzTEajaClOj4Gjo7OSgc31690"
            className={profileClasses.profileImage}
          />
        </Grid>
        <Grid item xs={12} sm={6} className={profileClasses.profileSection}>
          <div className={profileClasses.profileInfo}>
            <h4 className={profileClasses.profileAvailability}>FULL-TIME NANNY</h4>
            <h2 className={profileClasses.profileTitle}>Abigail G.</h2>
            <div className={profileClasses.profileRating}>
              <span className={profileClasses.ratingStars}>
                <Rating name="size-large" defaultValue={2} readOnly />
              </span>
              <span className={profileClasses.reviewCount}>(8)</span>
              <span className={profileClasses.hireCount}>Cared for 2 families</span>
            </div>
            <div className={profileClasses.profileDetails}>
              $20 – 30/hr <span className={profileClasses.bullet}>•</span> Leesburg, VA
            </div>
            <div className={profileClasses.profileDetails}>
              10+ yrs exp <span className={profileClasses.bullet}>•</span> 35 yrs old
            </div>
            <Divider />
            <Button variant="contained" className={profileClasses.joinBtn}>
              <span>Join to contact</span>
            </Button>
          </div>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Profile;
