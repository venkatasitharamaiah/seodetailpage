import { theme } from '@care/material-ui-theme';
import { Container, makeStyles } from '@material-ui/core';
import React from 'react';

const profileDescStyles = makeStyles(() => ({
  descriptionSection: {
    paddingTop: '24px',
  },
  descriptionHeading: {
    color: '#172C42',
    marginTop: '20px',
    fontSize: '22px',
    lineHeight: '28px',
    fontWeight: 'bold',
    margin: 0,
    letterSpacing: '1px',
    marginBottom: '20px',

    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
  descriptionText: {
    transition: 'max-height 0.5s ease-in',
    maxHeight: '1000px',
    overflow: 'hidden',
    fontSize: '20px',
    lineHeight: '28px',
    WebkitFontSmoothing: 'antialiased',
    letterSpacing: '1px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '15px',
    },
  },
}));

const ProfileDescription = () => {
  const descClasses = profileDescStyles();
  return (
    <>
      <Container maxWidth="md">
        <div className={descClasses.descriptionSection}>
          <h4 className={descClasses.descriptionHeading}>Meet Abigail</h4>
          <div className={descClasses.descriptionText}>
            Hello! Thanks for giving me your time. My name is Abby and I am a nanny/home organizer.
            I love my job for so many reasons - it is a way to help others live their best life, it
            funds future education goals, and brings me joy! :) Children are such a joy in my life,
            and I like to keep my days full of joy! As far as hobbies go, I love to cook, read,
            paint and spend LOTS of time outdoors! I would find it a pleasure to enjoy these hobbies
            with your kids, as well as to encourage them in their own interests. I am so grateful to
            my childhood caregivers for ensuring my safe and stable environment - it is my goal to
            ensure the same for your little ones! Part of this stable environment is maintaining a
            tidy home and keeping exposure to devices at a minimum. I will work extra hard to keep a
            clean house, assist your family with errands, and entertain and encourage your children
            in wholesome activities.
          </div>
        </div>
      </Container>
    </>
  );
};

export default ProfileDescription;
