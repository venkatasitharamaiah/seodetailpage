import React from 'react';
import { Grid, Button, Avatar, Container } from '@material-ui/core';
import { Rating, Typography } from '@care/react-component-lib';
import recentReviewStyles from '../styles/recentReview.style';

const RecentReview = () => {
  const reviewClasses = recentReviewStyles();

  return (
    <Container maxWidth="md">
      <div className={reviewClasses.reviewSection}>
        <h4 className={reviewClasses.reviewText}>Most recent review</h4>
        <div className={reviewClasses.reviewInfo}>
          <Grid container>
            <Grid item xs={3}>
              <Avatar className={reviewClasses.reviewPhoto}>JT</Avatar>
            </Grid>
            <Grid item xs={6}>
              <div className={reviewClasses.reviewerName}>Jeff T. Hired Abigail</div>
              <div>
                <span>
                  <Rating name="size-large" defaultValue={2} readOnly />
                </span>
                <span className={reviewClasses.reviewDate}>Jul 20, 2020</span>
              </div>
            </Grid>
          </Grid>
        </div>
        <div className={reviewClasses.reviewBody}>
          <Typography className={reviewClasses.reviewerContent}>
            Abby is extraordinary and feels like a part of our family. She is extremely talented
            with children, providing thoughtful and engaging activities for our three kiddos
            throughout the day. Our kids are truly attached to her and look forward to our Abby days
            like they are heading to Disney. Abby does a great job integrating our input into her
            approach childcare, making her work for us a true partnership. We are very lucky to have
            her and recommend her unreservedly.
          </Typography>
        </div>
        <div className={reviewClasses.reviewCount}>Showing results 1 of 3</div>
        <div className={reviewClasses.readAllReviews}>
          <Button variant="outlined" className={reviewClasses.readAllReviewsBtn}>
            <span>Read all reviews</span>
          </Button>
        </div>
      </div>
    </Container>
  );
};

export default RecentReview;
