import { Container, Grid, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import { Rating } from '@material-ui/lab';
import { theme } from '@care/material-ui-theme';

const careGivers = [
  {
    id: 1,
    profilePic:
      'https://d1z3744m6z2h75.cloudfront.net/aws/photo/160X120/2/11479102_8YwCP7CAQVKEuvjxo6xpWwr5xAj7Z480',
    name: 'Karen V.',
    location: 'Louisburg',
    hasStars: true,
  },
  {
    id: 2,
    profilePic:
      'https://d1z3744m6z2h75.cloudfront.net/aws/photo/160X120/77/39293377_MZQUrGdn6M5jZGjle0XQDuWbHUy010',
    name: 'Erin M.',
    location: 'Louisburg',
    hasStars: false,
  },
  {
    id: 3,
    profilePic:
      'https://d1z3744m6z2h75.cloudfront.net/aws/photo/160X120/48/33898748_ruSfWPFzJFGHchIXvrbnUa0bDyyn00',
    name: 'Madu R.',
    location: 'Louisburg',
    hasStars: true,
  },
  {
    id: 4,
    profilePic:
      'https://d1z3744m6z2h75.cloudfront.net/aws/photo/160X120/75/17778175_Tjnu5TzkXG9exRyps43aiFmV7cn010',
    name: 'Janeth H.',
    location: 'Louisburg',
    hasStars: false,
  },
];

const similarProfileStyles = makeStyles(() => ({
  similarProfileInfo: {
    paddingTop: '24px',
  },
  careGiverSection: {
    marginTop: '30px',
    display: 'flex',
    justifyContent: 'space-evenly',

    [theme.breakpoints.up('sm')]: {
      display: 'inline-block',
      textAlign: 'center',
    },
  },
  careGiverProfileImg: {
    display: 'block',

    [theme.breakpoints.down('xs')]: {
      display: 'inline-block',
    },
  },
  careGiverProfileInfo: {
    display: 'block',

    [theme.breakpoints.down('xs')]: {
      display: 'inline-block',
    },
  },
  careGiverName: {
    marginTop: 0,
    fontSize: '20px',
  },
  careGiverAvatar: {
    width: '100px',
    height: '100px',
    borderRadius: '50%',
  },
}));

const SimilarProfiles = () => {
  const similarProfiles = similarProfileStyles();

  return (
    <Container maxWidth="md">
      <div className={similarProfiles.similarProfileInfo}>
        <h2>Similar caregivers nearby</h2>
        <Grid container>
          {careGivers.map((careGiver) => {
            return (
              <Grid item xs={12} sm={2}>
                <div className={similarProfiles.careGiverSection}>
                  <div className={similarProfiles.careGiverProfileImg}>
                    <img
                      src={careGiver.profilePic}
                      className={similarProfiles.careGiverAvatar}
                      alt="avatar"
                    />
                  </div>
                  <div className={similarProfiles.careGiverProfileInfo}>
                    <h2 className={similarProfiles.careGiverName}>{careGiver.name}</h2>
                    <div>
                      {careGiver.hasStars ? (
                        <Rating name="size-large" defaultValue={2} readOnly />
                      ) : (
                        <Rating name="size-large" defaultValue={0} readOnly />
                      )}
                    </div>
                    <Typography>{careGiver.location}</Typography>
                  </div>
                </div>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </Container>
  );
};

export default SimilarProfiles;
