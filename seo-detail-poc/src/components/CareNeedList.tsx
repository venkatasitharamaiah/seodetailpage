import React from 'react';
import { Button, Grid } from '@material-ui/core';
import Link from 'next/link';
import careNeedStyles from '../styles/careNeed.style';

const CareNeedList = () => {
  const conversionClasses = careNeedStyles();

  return (
    <>
      <div className={conversionClasses.seoConversion}>
        <Grid container>
          <Grid item xs={12} className={conversionClasses.seoConversionTitle}>
            <h2>When do you need a nanny?</h2>
          </Grid>
          <Grid item xs={12}>
            <Grid container className={conversionClasses.seoConversionCtas}>
              <Grid item xs={6} sm={4} md={2}>
                <Link href="/">
                  <Button variant="contained" className={conversionClasses.conversionBtn}>
                    Right Now
                  </Button>
                </Link>
              </Grid>
              <Grid item xs={6} sm={4} md={2}>
                <Link href="/">
                  <Button variant="contained" className={conversionClasses.conversionBtn}>
                    Within a week
                  </Button>
                </Link>
              </Grid>
              <Grid item xs={6} sm={4} md={2}>
                <Link href="/">
                  <Button variant="contained" className={conversionClasses.conversionBtn}>
                    In 1-2 months
                  </Button>
                </Link>
              </Grid>
              <Grid item xs={6} sm={4} md={2}>
                <Link href="/">
                  <Button variant="contained" className={conversionClasses.conversionBtn}>
                    Just browsing
                  </Button>
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default CareNeedList;
