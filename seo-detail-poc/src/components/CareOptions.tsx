import { theme } from '@care/material-ui-theme';
import { Container, makeStyles } from '@material-ui/core';
import Link from 'next/link';
import React from 'react';

const careOptionsStyles = makeStyles(() => ({
  careOptions: {
    paddingTop: '24px',
  },
  careOptionsText: {
    fontSize: '21px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '16px',
    },
  },
  careOptionsSection: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '100px',

    [theme.breakpoints.down('xs')]: {
      display: 'grid',
    },
  },
  careOptionsName: {
    fontSize: '20px',
    color: '#015AC8',
    textDecoration: 'none',
    display: 'inline-block',
    cursor: 'pointer',

    [theme.breakpoints.down('xs')]: {
      lineHeight: '45px',
      fontSize: '19px',
    },
  },
  careOptionsSeparator: {
    display: 'inline-block',
    fontSize: '20px',
    padding: '0px 8px',

    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },

    '&:last-child': {
      visibility: 'hidden',
    },
  },
}));

const optionsList = [
  { id: 1, name: 'Babysitters' },
  { id: 2, name: 'Nannies' },
  { id: 3, name: 'Tutors' },
  { id: 4, name: 'Housekeepers' },
  { id: 5, name: 'Pet Care' },
  { id: 6, name: 'Senior Care' },
];

const CareOptions = () => {
  const descClasses = careOptionsStyles();
  return (
    <Container maxWidth="md">
      <div className={descClasses.careOptions}>
        <h4 className={descClasses.careOptionsText}>Or take a look at some other care options:</h4>
        <div className={descClasses.careOptionsSection}>
          {optionsList.map((option) => {
            return (
              <>
                <Link href="/" key={option.id}>
                  <span className={descClasses.careOptionsName}>{option.name}</span>
                </Link>
                <span className={descClasses.careOptionsSeparator}>|</span>
              </>
            );
          })}
        </div>
      </div>
    </Container>
  );
};

export default CareOptions;
