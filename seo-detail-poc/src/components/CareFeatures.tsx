import { theme } from '@care/material-ui-theme';
import { IconTickLg } from '@care/react-icons';
import { Container, Grid, Hidden, List, ListItem, makeStyles } from '@material-ui/core';
import React from 'react';

const styles = makeStyles(() => ({
  featureInfo: {
    paddingTop: '24px',

    [theme.breakpoints.down('xs')]: {
      paddingTop: '0px',
    },
  },
  featureList: {
    margin: '15px 0px',
    lineHeight: '26px',
    paddingLeft: '0px',
    paddingRight: '0px',

    '&:hover': {
      backgroundColor: '#ffffff !important',
    },

    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      justifyContent: 'space-between',
    },
  },
  leftTickIcon: {
    width: '19px',
    height: '19px',
  },
  rightTickIcon: {
    width: '19px',
    height: '19px',

    [theme.breakpoints.down('xs')]: {
      width: '17px',
      height: '17px',
    },
  },
  featureText: {
    fontSize: '19px',
    paddingLeft: '7px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '16px',
      paddingLeft: '0px',
    },
  },
}));

const CareFeatures = ({ features, featureFor }) => {
  const featureClasses = styles();
  return (
    <>
      <Container maxWidth="md">
        <h2
          style={featureFor === 'verifiedInfo' ? { display: 'inline-block' } : { display: 'none' }}>
          VerifiedInfo
        </h2>
        <div className={featureClasses.featureInfo}>
          <List>
            <Grid container>
              {features.map((feature) => {
                return (
                  <Grid item xs={12} sm={6}>
                    <div key={feature.id}>
                      <ListItem className={featureClasses.featureList}>
                        <Hidden xsDown>
                          <IconTickLg className={featureClasses.leftTickIcon} />
                        </Hidden>
                        <div className={featureClasses.featureText}>{feature.name}</div>
                        <Hidden smUp>
                          <IconTickLg className={featureClasses.rightTickIcon} />
                        </Hidden>
                      </ListItem>
                    </div>
                  </Grid>
                );
              })}
            </Grid>
          </List>
        </div>
      </Container>
    </>
  );
};

export default CareFeatures;
