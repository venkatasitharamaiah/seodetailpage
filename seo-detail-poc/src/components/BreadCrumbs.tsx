import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Breadcrumbs, Container } from '@material-ui/core';
import Link from 'next/link';
import { Typography } from '@care/react-component-lib';

const styles = makeStyles(() => ({
  breadCrumbSection: {
    paddingTop: '24px',
  },
  breadcrumbText: {
    fontSize: '18px !important',
    color: '#505D6D',
  },
}));

const BreadCrumbs = () => {
  const breadcrumbClasses = styles();

  return (
    <Container maxWidth="md">
      <div className={breadcrumbClasses.breadCrumbSection}>
        <Breadcrumbs aria-label="breadcrumb">
          <Link href="/">
            <span className={breadcrumbClasses.breadcrumbText}>Child Care</span>
          </Link>
          <Link href="/">
            <span className={breadcrumbClasses.breadcrumbText}>Child Care in Leesburg, VA</span>
          </Link>
          <Typography>Abigail G.</Typography>
        </Breadcrumbs>
      </div>
    </Container>
  );
};

export default BreadCrumbs;
