import { Typography } from '@material-ui/core';
import Link from 'next/link';
import React from 'react';

export default function Home() {
  return (
    <>
      <Link href="/seo-detail-page">
        <Typography style={{ cursor: 'pointer' }}>Go to Seo Detail Page</Typography>
      </Link>
    </>
  );
}
