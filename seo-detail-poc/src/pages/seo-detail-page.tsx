import React from 'react';
import { Button, Container, Grid, makeStyles, Typography } from '@material-ui/core';
import { theme } from '@care/material-ui-theme';
import { CoreGlobalFooter } from '@care/navigation';
import CareNeedList from '../components/CareNeedList';
import Profile from '../components/Profile';
import ProfileDescription from '../components/ProfileDescription';
import BreadCrumbs from '../components/BreadCrumbs';
import CareFeatures from '../components/CareFeatures';
import CustomDivider from '../components/CustomDivider';
import SideJobs from '../components/SideJobs';
import RecentReview from '../components/RecentReview';
import CareOptions from '../components/CareOptions';
import SimilarProfiles from '../components/SimilarProfiles';
import SeoGlobalNavbar from '../components/SeoGlobalNavbar';

const features = [
  { id: 1, name: 'Comfortable with pets' },
  { id: 2, name: 'Non smoker' },
  { id: 3, name: 'Has transportation' },
  { id: 4, name: 'Willing to travel 15 mi' },
  { id: 5, name: 'CPR Trained' },
];

const verifiedInfo = [
  { id: 1, name: 'Phone Verified' },
  { id: 2, name: 'Email Verified' },
  { id: 3, name: 'Facebook Verified' },
];

const styles = makeStyles(() => ({
  educationText: {
    fontSize: '26px',
  },
  collegeName: {
    fontSize: '18px',
    marginBottom: '16px',
  },
  joinFreeBtnSection: {
    paddingTop: '24px',
  },
  joinFreeBtnBody: {
    textAlign: 'center',
  },
  joinFreeBtn: {
    color: '#ffffff',
    backgroundColor: '#EF5844',
    borderColor: '#EF5844',
    width: '350px',
    height: '55px',
    borderRadius: '40px',

    '&:hover': {
      backgroundColor: '#EF5844',
      borderColor: '#EF5844',
      opacity: '80%',
    },

    [theme.breakpoints.down('xs')]: {
      width: '280px',
      height: '50px',
    },
  },
  joinFreeBtnText: {
    fontSize: '20px',
  },
}));

const SeoDetailPage = () => {
  const detailClasses = styles();
  return (
    <>
      <SeoGlobalNavbar />
      <Grid container>
        <Grid item xs={12}>
          <Profile />
        </Grid>
        <Grid item xs={12}>
          <CareNeedList />
        </Grid>
        <Grid item xs={12}>
          <BreadCrumbs />
        </Grid>
        <Grid item xs={12}>
          <ProfileDescription />
        </Grid>
        <Grid item xs={12}>
          <CareFeatures features={features} featureFor="features" />
        </Grid>
        <CustomDivider />
        <Grid item xs={12}>
          <SideJobs />
        </Grid>
        <CustomDivider />
        <Grid item xs={12}>
          <RecentReview />
        </Grid>
        <CustomDivider />
        <Grid item xs={12}>
          <Container maxWidth="md">
            <h2 className={detailClasses.educationText}>Education</h2>
            <div>
              <h5 className={detailClasses.collegeName}>HOUSTON COLLEGE</h5>
              <Typography>Some college</Typography>
            </div>
          </Container>
        </Grid>
        <CustomDivider />
        <Grid item xs={12}>
          <CareFeatures features={verifiedInfo} featureFor="verifiedInfo" />
        </Grid>
        <Grid item xs={12}>
          <div className={detailClasses.joinFreeBtnSection}>
            <div className={detailClasses.joinFreeBtnBody}>
              <Button variant="contained" className={detailClasses.joinFreeBtn}>
                <span className={detailClasses.joinFreeBtnText}>Join free to see full profile</span>
              </Button>
            </div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <SimilarProfiles />
        </Grid>
        <Grid item xs={12}>
          <CareOptions />
        </Grid>
      </Grid>
      <CoreGlobalFooter />
    </>
  );
};

export default SeoDetailPage;
