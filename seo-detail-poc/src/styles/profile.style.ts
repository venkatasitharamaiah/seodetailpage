import { makeStyles } from '@material-ui/core/styles';
import { theme } from '@care/material-ui-theme';

const profileStyles = makeStyles(() => ({
  profileSection: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileImage: {
    width: '350px',
    height: '350px',
    borderRadius: '5%',

    [theme.breakpoints.down('xs')]: {
      width: '150px',
      height: '150px',
      borderRadius: '50%',
      marginTop: '50px',
    },
  },
  profileInfo: {
    margin: '58px 0 67px 0',
    width: '290px',
    textAlign: 'left',

    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      margin: '35px 0 50px 0',
    },
  },
  profileAvailability: {
    fontSize: '16px',
    textTransform: 'uppercase',
    fontWeight: 'bold',
    webkitFontSmoothing: 'antialiased',
    lineHeight: '18px',
    letterSpacing: '0.5px',
    color: '#505d6d',
  },
  profileTitle: {
    color: '#172c42',
    marginTop: '20px',
    marginBottom: '10px',
    fontSize: '28px',
    lineHeight: '38px',
    fontWeight: 'bold',
    margin: 0,

    [theme.breakpoints.down('xs')]: {
      marginTop: '10px',
    },
  },
  profileRating: {
    marginTop: '8px',
    marginBottom: '16px',
    display: 'flex',
    alignItems: 'baseline',

    [theme.breakpoints.up('sm')]: {
      marginBottom: '24px',
    },
  },
  ratingStars: {
    fontSize: '16px',
  },
  reviewCount: {
    color: '#505d6d',
    fontSize: '16px',
    verticalAlign: 'sub',
    marginLeft: '8px',

    [theme.breakpoints.up('sm')]: {
      verticalAlign: 'middle',
    },
  },
  hireCount: {
    color: '#505d6d',
    fontSize: '15px',
    marginTop: '8px',
    lineHeight: '17px',

    [theme.breakpoints.up('sm')]: {
      display: 'inline-block',
      verticalAlign: 'middle',
      marginTop: '4px',
      marginLeft: '8px',
    },
  },
  profileDetails: {
    color: '#172c42',
    letterSpacing: '.5px',
    fontSize: '20px',
    fontWeight: 'bold',
    marginTop: 0,
    marginBottom: '8px',
    lineHeight: '30px',
  },
  bullet: {
    color: '#667483',
    fontSize: '25px',
    verticalAlign: 'top',
  },
  joinBtn: {
    color: '#ffffff',
    backgroundColor: '#EF5844',
    borderColor: '#EF5844',
    width: '66%',
    height: '46px',
    borderRadius: '30px',
    marginBottom: '25px',
    marginTop: '8px',
  },
}));

export default profileStyles;
