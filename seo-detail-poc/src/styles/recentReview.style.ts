import { theme } from '@care/material-ui-theme';
import { makeStyles } from '@material-ui/core/styles';

const recentReviewStyles = makeStyles(() => ({
  reviewSection: {
    paddingTop: '24px',
  },
  reviewText: {
    fontSize: '23px',
    marginTop: '0px',

    [theme.breakpoints.down('xs')]: {
      textAlign: 'center',
    },
  },
  reviewInfo: {
    width: '400px',
  },
  reviewPhoto: {
    width: '85px',
    height: '85px',
  },
  reviewerName: {
    fontSize: '20px',
    marginBottom: '7px',
    marginTop: '10px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '18px',
    },
  },
  reviewDate: {
    fontSize: '18px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '16px',
    },
  },
  reviewBody: {
    margin: '20px 0px',
  },
  reviewerContent: {
    fontSize: '20px',
    lineHeight: '30px',
    letterSpacing: '1px',
    margin: '35px 0px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '15px',
    },
  },
  reviewCount: {
    color: '#505D6D',
    fontSize: '20px',
  },
  readAllReviews: {
    textAlign: 'center',
    marginTop: '40px',
  },
  readAllReviewsBtn: {
    borderRadius: '30px',
    minWidth: '220px',
    width: '220px',
    height: '47px',
  },
}));

export default recentReviewStyles;
