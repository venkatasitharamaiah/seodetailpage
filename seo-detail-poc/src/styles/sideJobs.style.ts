import { theme } from '@care/material-ui-theme';
import { makeStyles } from '@material-ui/core/styles';

const sideJobsStyles = makeStyles(() => ({
  sectionTitle: {
    fontSize: '23px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '22px',
    },
  },
  sideJobsSection: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  sideJobs: {
    marginBottom: '20px',
    marginTop: '10px',
  },
  sideJob: {
    border: '1px solid #33465A',
    width: '65px',
    height: '65px',
    margin: '0 auto',
    borderRadius: '50%',
  },
  sideJobIcon: {
    margin: '20px',
  },
  sideJobName: {
    fontSize: '16px',
    color: '#172C42',
    marginTop: '5px',
    textAlign: 'center',
  },
}));

export default sideJobsStyles;
