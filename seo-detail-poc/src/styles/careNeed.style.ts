import { theme } from '@care/material-ui-theme';
import { makeStyles } from '@material-ui/core/styles';

const careNeedStyles = makeStyles(() => ({
  seoConversion: {
    paddingTop: '24px',
    paddingBottom: '32px',
    backgroundColor: '#e6e8ea',

    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
    },
  },
  seoConversionTitle: {
    marginBottom: '35px',
    textAlign: 'center',
    fontSize: '18px',

    [theme.breakpoints.down('xs')]: {
      fontSize: '15px',
    },
  },
  seoConversionCtas: {
    display: 'flex',
    justifyContent: 'space-evenly',
    flexWrap: 'wrap',

    [theme.breakpoints.up('lg')]: {
      justifyContent: 'center',
    },
  },
  conversionBtn: {
    color: '#ffffff',
    backgroundColor: '#33465A',
    borderColor: '#33465A',
    borderRadius: '70px',
    fontSize: '18px',
    minHeight: '44px',
    lineHeight: '20px',
    margin: '0px 20px',
    padding: '10px 24px 7px 24px',
    minWidth: '200px',
    marginBottom: '20px',

    '&:hover': {
      backgroundColor: '#000000',
    },

    [theme.breakpoints.down('xs')]: {
      margin: '12px 5px',
      padding: '5px 5px 5px 5px',
      fontSize: '17px',
      minWidth: '170px',
      minHeight: '40px',
    },
  },
}));

export default careNeedStyles;
